﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextSwitcher : MonoBehaviour {

	[SerializeField] private string[] prompts;

	void OnEnable()
	{
		ChangeText();
	}

	void ChangeText()
	{
		int textIndex = Random.Range(0,prompts.Length);
		GetComponent<Text>().text = prompts[textIndex];
	}
}
