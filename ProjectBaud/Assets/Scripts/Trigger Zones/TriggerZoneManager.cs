﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZoneManager : MonoBehaviour {

    public TriggerZone outerRing;
    public TriggerZone middleRing;
    public TriggerZone innerRing;

    [Range(0,1)] public float outerAlpha;
    [Range(0,1)] public float middleAlpha;
    [Range(0,1)] public float innerAlpha;

    private void Update()
    {
        //if(innerRing.isTriggered)
        //{
        //    innerRing.other.GetComponent<PlayerColor>().SetPlayerAlpha(innerAlpha);
        //}
        //else if(middleRing.isTriggered)
        //{
        //    middleRing.other.GetComponent<PlayerColor>().SetPlayerAlpha(middleAlpha);
        //}
        //else if(outerRing.isTriggered)
        //{
        //    outerRing.other.GetComponent<PlayerColor>().SetPlayerAlpha(outerAlpha);
        //}
    }

}
