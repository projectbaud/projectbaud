﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerUI : MonoBehaviour {

    private Text targetText;

    private void Awake()
    {
        targetText = GetComponent<Text>();
    }

    private void Update()
    {
        //if(GetComponentInParent<MenuSystemManager>().GetCurrentMenu()== "Rese")
        targetText.text = ((int) GameManager.timer).ToString();
    }

}
