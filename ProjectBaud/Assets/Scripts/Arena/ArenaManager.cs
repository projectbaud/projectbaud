﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArenaManager : MonoBehaviour {

    public GameObject triggerPrefab;
    public AnimationCurve triggerCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

    public GameObject bottomLeftArenaIndicator;
    public GameObject topRightArenaIndicator;

	public static ArenaManager instance;

    public GameObject zoneFolder;

    private void Start()
    {
        SetTriggerPlacements();
    }

	private void Awake()
	{
		instance = this;
	}

    private void SetTriggerPlacements()
    {
        ClearZones();

		bool[] assigned = new bool[4];
		assigned[0] = false;
		assigned[1] = false;
		assigned[2] = false;
		assigned[3] = false;

		for (int i = 0; i < 4; i++)
        {
            float signalPoint = UnityEngine.Random.Range((0.25f*i), (0.25f*i)+0.25f);

			//Debug.Log(i + " : " + signalPoint);

            float xPos = Mathf.Lerp(bottomLeftArenaIndicator.transform.position.x, topRightArenaIndicator.transform.position.x, signalPoint);
            float yPos = Mathf.Lerp(bottomLeftArenaIndicator.transform.position.y, topRightArenaIndicator.transform.position.y, triggerCurve.Evaluate(signalPoint));

            Vector3 targetPosition = new Vector3(xPos, yPos);
            GameObject newTrigger = (GameObject) Instantiate(triggerPrefab, targetPosition, transform.rotation);

            newTrigger.transform.SetParent(zoneFolder.transform);

			int randomPlayer = (int)Random.Range(0, 3.999f);
			while (assigned[randomPlayer] == true) {
				randomPlayer = (int)Random.Range(0, 3.999f);
			}
			assigned[randomPlayer] = true;
			//Debug.Log("random player " + randomPlayer);
			newTrigger.GetComponentInChildren<TriggerZone>().targetPlayerNumber = randomPlayer;

			GameManager.triggers[randomPlayer] = newTrigger;
		}
    }

    private void ClearZones()
    {
        for(int i = zoneFolder.transform.childCount-1; i >= 0; i--)
        {
			GameManager.triggers = new GameObject[4];
            Destroy(zoneFolder.transform.GetChild(i).gameObject);
        }
    }
}
