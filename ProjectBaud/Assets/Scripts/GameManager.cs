﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	//initialize variables
	public static bool[] correctPosition;//when these all equal true you win the game
	public static int zonesActive;
	public static GameManager instance;//this is what will make this persist
	public static string currentLevel;//this is the current scene that you are playing
	public static float timer;//this is the timer in the level
	public static float winTimer;//this is the timer between winning and the win screen popping up

	public static AnimationCurve levelWave;
	public static GameObject[] triggers = new GameObject[4];

	public static int[] orderOfPlayers;



	void Awake(){
		//when the scene starts if there is a game manager already in memory keep it and destroy the new one
		if (instance == null) {
			instance = this;
		} else {
			Destroy(this.gameObject);
		}
	}
		
	// Use this for initialization
	void Start () {
		correctPosition = new bool[4];//reset the win condition
		currentLevel = SceneManager.GetActiveScene().name;//set the current level
		timer = 0.0f;//reset the timer
		winTimer = 0.0f;//reset the win timer
		Time.timeScale = 1;
		if (currentLevel != "Main Menu") {			
			levelWave = ArenaManager.instance.triggerCurve;
		}
	}


	// Update is called once per frame
	void Update () {
		
		//timer of the level- only runs if the scene is a level
		if (currentLevel != "Main Menu") {
			timer += Time.deltaTime;
			//UI timer update
//			Debug.Log(menuCanvas.GetComponentInChildren<Text>("Number").text);
//			menuCanvas.GetComponentInChildren<Text>("Number").text = (int)timer;
		}

		if (Input.GetKeyDown (KeyCode.Escape)) {
			PauseGame ();
		}

		//the player scripts set these to true when you are in the correct place, if all players are in the right place you win the game
		if (correctPosition[0] && correctPosition[1] && correctPosition[2] && correctPosition[3]) {

			Debug.Log ("you won yay");

			for (int i = 0; i < triggers.Length; i++) {
				triggers[i].GetComponentInChildren<ParticleSystem> ().Play ();
				Debug.Log ("playing the particles");
			}

			winTimer++;

			if(winTimer == 30){
				ResultsScreen ();
			}
		}
	}


	public void ResultsScreen(){
		Time.timeScale = 0;
		//enable results screen
		MenuSystemManager.instance.SwitchMenu("Results");
	}


	public void NextScene (){
		Time.timeScale = 1;
		if (currentLevel == "Main Menu"){
			SceneManager.LoadScene ("Square");
		} else if (currentLevel == "Square") {
			SceneManager.LoadScene ("Sawtooth");
		} else if (currentLevel == "Sawtooth") {
			SceneManager.LoadScene ("Sine");
		} else if (currentLevel == "Sine") {
			SceneManager.LoadScene ("Square");
		}
	}	


	public void PauseGame (){
		Time.timeScale = 0;
		//set pause menu to active
		MenuSystemManager.instance.SwitchMenu("Pause");
	}

	public void ResumeGame(){
		Time.timeScale = 1;
		MenuSystemManager.instance.SwitchMenu ("HUD");
	}

	public void LoadMenu (){
		Time.timeScale = 1;
		SceneManager.LoadScene ("Main Menu");
	}


	public void BackToMenu(){
		MenuSystemManager.instance.SwitchMenu("Main Menu");
	}


	// OnSceneLoaded stuff.
	void OnEnable()	{ SceneManager.sceneLoaded += OnSceneLoaded; }
	void OnDisable() { SceneManager.sceneLoaded -= OnSceneLoaded; }
	void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
		//Debug.Log("Scene Loaded");
		//Debug.Log(scene.name);
		//Debug.Log(mode);
		currentLevel = SceneManager.GetActiveScene().name;//set the current level
		if (currentLevel != "Main Menu") {			
			levelWave = ArenaManager.instance.triggerCurve;
		}
	}
}