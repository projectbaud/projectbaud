﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EyeballController : MonoBehaviour {

	public GameObject eye;
	private float[] distances;
	private GameObject triggerToLookAt;
	public float lookSpeed;

	public Sprite[] sprites;

	public SpriteRenderer spriteRenderer;

	void Start () {
		spriteRenderer.sprite = sprites[19];
	}
	
	void Update () {

		distances = new float[4];
		triggerToLookAt = null;

		float shortestDistance = 10000000;

		for (int i = 0; i < GameManager.triggers.Length; i++) {
			distances[i] = Vector2.Distance(GameManager.triggers[i].transform.position, transform.position);

			if (distances[i] < shortestDistance) {
				triggerToLookAt = GameManager.triggers[i];
				shortestDistance = distances[i];
			}
		}

		var newRotation = Quaternion.LookRotation(triggerToLookAt.transform.position - transform.position, -Vector3.forward);
		newRotation.x = 0.0f;
		newRotation.y = 0.0f;
		eye.transform.rotation = Quaternion.Slerp(eye.transform.rotation, newRotation, Time.deltaTime * 8);

		float ratio = shortestDistance/2;
		//Debug.Log("step1 " + ratio);
		if (ratio > 1) {
			ratio = 1;
		}
		//Debug.Log("step2 " + ratio);
		//ratio = Math.Abs(ratio - 1);
		//Debug.Log("step3 " + ratio);
		int frame = (int)Math.Round(ratio * 19);
		//Debug.Log("step4 " + frame);

		if (frame > 19) {
			frame = 19;
		}

		spriteRenderer.sprite = sprites[frame];
		//Debug.Log("==========");

	}

}
