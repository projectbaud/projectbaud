﻿using UnityEngine;
using System.Collections;
using Rewired;
using UnityEngine.Events;

public class UIInputMenuItem : MonoBehaviour {

	[Range(1,4)] public int playerNumber = 1;
    public bool allPlayers;
    public bool allControllers;
	public Player targetPlayer;
	public string inputName;

	public float deadzone = .1f;
	public float inputActionsDuration = .2f;
	public UnityEvent OnButtonDown;
	public UnityEvent OnPositiveAxis;
	public UnityEvent OnNegativeAxis;
	public UnityEvent OnIdleAxis;
    public UnityEvent OnAnyButtonDown;

    private int targetInputIndex;

	private float timer;

	void Awake()
	{
		timer = Time.time-inputActionsDuration;
	}

	void OnEnable()
	{
		targetPlayer = ReInput.players.Players[playerNumber - 1];
	}

    void Update()
    {
        if (allControllers)
        {
            foreach(Controller controller in ReInput.controllers.Controllers)
            {
                CheckInputs(controller);
            }
        }
        if (allPlayers)
        {
            foreach (Player player in ReInput.players.AllPlayers)
            {
                CheckInputs(player);
            }
        }
        else
        {
            CheckInputs(targetPlayer);
        }
	}

    void CheckInputs(Player player)
    {
        if (player.GetButtonDown(inputName))
        {
            OnButtonDown.Invoke();
        }

        if (player.GetAxisRaw(inputName) > 0 && CheckMovement(player))
        {
            OnPositiveAxis.Invoke();
        }

        if (player.GetAxisRaw(inputName) < 0 && CheckMovement(player))
        {
            OnNegativeAxis.Invoke();
        }

        if (!CheckMovement(player))
        {
            OnIdleAxis.Invoke();
        }

        if (player.GetAnyButtonDown())
        {
            OnAnyButtonDown.Invoke();
        }
    }

    void CheckInputs(Controller controller)
    {
        //if (controller.GetButtonDown(inputName))
        //{
        //    OnButtonDown.Invoke();
        //}

        //if (controller.GetAxisRaw(inputName) > 0 && CheckMovement(controller))
        //{
        //    OnPositiveAxis.Invoke();
        //}

        //if (controller.GetAxisRaw(inputName) < 0 && CheckMovement(controller))
        //{
        //    OnNegativeAxis.Invoke();
        //}

        //if (!CheckMovement(controller))
        //{
        //    OnIdleAxis.Invoke();
        //}

        if(controller.GetAnyButtonDown())
        {
            OnAnyButtonDown.Invoke();
        }
    }

	bool CheckMovement(Player player)
	{
		float moveFloat = player.GetAxisRaw(inputName);

		bool allow;
		
		if((moveFloat >= deadzone || moveFloat <= -deadzone) && (Time.time-timer >= inputActionsDuration))
		{
			allow = true;
			timer = Time.time;
		}
		else
		{
			allow = false;
		}

		return allow;
	}
}
