﻿using UnityEngine;
using System.Collections;

public class UIWebsite : MonoBehaviour {
	
	public void GoToLink(string website)
	{
		Application.OpenURL(website);
	}
}
