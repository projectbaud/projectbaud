﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;

public class SelectableStates : MonoBehaviour , ISelectHandler, IPointerClickHandler, ISubmitHandler {

    public UnityEvent OnSelected;
    public UnityEvent OnConfirmed;

    private Selectable targetSelectable;

    void Awake()
    {
        targetSelectable = GetComponent<Selectable>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(targetSelectable.interactable) OnSubmit(eventData);
    }

    public void OnSelect(BaseEventData eventData)
    {
        if (targetSelectable.interactable) OnSelected.Invoke();
    }

    public void OnSubmit(BaseEventData eventData)
    {
        if (targetSelectable.interactable) OnConfirmed.Invoke();
    }
}
