﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.Audio;
using System;
using UnityEngine.SceneManagement;

public class PlayerInteraction : MonoBehaviour {

    public int playerNumber = 0;
    protected PlayerTrigger triggers;

	public float waveMotionMultiplier = 1;

	public GameObject waveMovedObject;

	// Movement vars
	public int speed;
	private Vector2 targetVector;

	// Exposing Audio Mixer Objects
	public AudioMixer mixer;
	public AudioMixerSnapshot step0;
	public AudioMixerSnapshot step1;
	public AudioMixerSnapshot step2;
	public AudioMixerSnapshot step3;
	public AudioMixerSnapshot step4;

	public AudioSource sfxCollide;
	public AudioSource sfxInPosition;
	public AudioSource sfxInCorrectPosition;

	private float animationOffset;
	private float timeStep;


	void Awake() {
		// Collision Triggers
        triggers = GetComponent<PlayerTrigger>();
        triggers.OnEnterTrigger.AddListener(OnTriggerZoneEnter);
		triggers.OnStayTrigger.AddListener(OnTriggerZoneStay);
		triggers.OnExitTrigger.AddListener(OnTriggerZoneExit);

		triggers.OnCollideTrigger.AddListener(OnCollide);
	}

	void Start() {
		//Debug.Log("trigger count " + GameManager.triggers.Length);
		GameObject triggerZone = GameManager.triggers[playerNumber];
		animationOffset = (triggerZone.transform.position.x + 16) / 16;   // The screen is 16 units wide. // Use Vector3.Lerp here, silly!
		//animationOffset = Mathf.Lerp(bottomLeftArenaIndicator.transform.position.x, topRightArenaIndicator.transform.position.x, triggerZone.transform.position.x);
		timeStep = 1-animationOffset;
        step0.TransitionTo(0.25f);
        //Debug.Log("offset " + animationOffset);
    }

	void OnEnable() { SceneManager.sceneLoaded += OnSceneLoaded; }
	void OnDisable() { SceneManager.sceneLoaded -= OnSceneLoaded; }
	void OnSceneLoaded(Scene scene, LoadSceneMode mode) {

	}

	void Update() {
        if (ReInput.players.Players[playerNumber].GetButtonDown("pause")) GameManager.instance.PauseGame();
	}

	void FixedUpdate() {

		// Movement
		Vector2 targetVector = new Vector2(ReInput.players.Players[playerNumber].GetAxisRaw("horizontal"), ReInput.players.Players[playerNumber].GetAxisRaw("vertical"));
		if (targetVector != Vector2.zero) {
			//Rigidbody2D body = GetComponent<Rigidbody2D>();
			//body.transform.Translate(targetVector * speed * Time.deltaTime);
			transform.Translate(targetVector * speed * Time.deltaTime);
			if (waveMotionMultiplier > 0) {
				waveMotionMultiplier -= 0.05f;
			}
		} else {
			if (waveMotionMultiplier < 1) {
				waveMotionMultiplier += 0.05f;
			}
		}

		// Scale animation tied to the level's waveform.
		//transform.localScale = new Vector3(0.75f + GameManager.levelWave.Evaluate(timeStep) / 4, 0.75f + GameManager.levelWave.Evaluate(timeStep) / 4, 1);
		float movement = (GameManager.levelWave.Evaluate(timeStep)-0.5f)*waveMotionMultiplier;
		waveMovedObject.transform.localPosition = new Vector3(0, movement);
		timeStep = timeStep + 0.005f;
		if (timeStep > 1) {
			timeStep = timeStep - 1;
		}
	}

	void waveMotionChange(int newWaveMotionMultiplier) {

	}

	private void OnCollide(GameObject gameObject) {
		// on collider with player
		Debug.Log("COLLIDE! step 2");
		sfxCollide.Play();
	}

	virtual protected void OnTriggerZoneEnter(int targetPlayer, GameObject zoneObject) {
		if (targetPlayer == playerNumber) {
			GameManager.zonesActive++;
			transitionToShapshot();
			//when the player enters the correct zone they should send true to the game manager
			GameManager.correctPosition[playerNumber] = true;
			Debug.Log("player " + playerNumber + " in position");
			sfxInCorrectPosition.Play();
		} else {
			sfxInPosition.Play();
		}
		// Trigger particle effect on zone when any player hits one.
		zoneObject.GetComponent<ParticleSystem>().Play();
	}

	virtual protected void OnTriggerZoneStay(int targetPlayer, GameObject zoneObject) { }

	virtual protected void OnTriggerZoneExit(int targetPlayer, GameObject zoneObject) {
		if (targetPlayer == playerNumber) {
			GameManager.zonesActive--;
			transitionToShapshot();
			//when the player exists the correct zone they should send false to the game manager
			GameManager.correctPosition[playerNumber] = false;
			Debug.Log("player " + playerNumber + " out of position");
		}
	}

	private void transitionToShapshot() {
        Debug.Log(GameManager.zonesActive);
		if (GameManager.zonesActive == 0) {
			step0.TransitionTo(0.25f);
		} else if (GameManager.zonesActive == 1) {
			step1.TransitionTo(0.25f);
		} else if (GameManager.zonesActive == 2) {
			step2.TransitionTo(0.25f);
		} else if (GameManager.zonesActive == 3) {
			step3.TransitionTo(0.25f);
		} else if (GameManager.zonesActive == 4) {
			step4.TransitionTo(0.25f);
		}
	}


}