﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Rewired;

public class EventSystemMultipleControls : MonoBehaviour {
	
	public Selectable firstSelection;
	public bool autoSelect;
	public float deadzone = .1f;
	public float inputActionsDuration = .2f;

	private Player targetPlayer;
    [Range(1,4)] public int playerNumber = 1;
    public bool allPlayers = false;
    //public int controllerNumber;

    public UnityEvent OnInputReceived;


    private Selectable[] selectables;

	private Selectable currentSelectedObject;

	private Selectable selectableUp;
	private Selectable selectableDown;
	private Selectable selectableLeft;
	private Selectable selectableRight;
	
	private float timer;

	void Awake()
	{
		selectables = GetComponentsInChildren<Selectable>();

		if (ReInput.players.Players[playerNumber - 1] != null)
		{
			targetPlayer = ReInput.players.Players[playerNumber - 1];
		}

		timer = Time.time-inputActionsDuration;
	}

	void OnEnable()
	{
		Invoke("PickFirstSelection", .1f);//PickFirstSelection();
	}

	void Update () {
		selectables = GetComponentsInChildren<Selectable>();

        if (allPlayers)
        {
            foreach (Player player in ReInput.players.Players)
            {
                
                if (player.GetButtonDown("Submit")) SendSubmitEventToSelectedObject();
            }
        }
        else
        {
            
            if (targetPlayer.GetButtonDown("Submit")) SendSubmitEventToSelectedObject();
        }

        CheckMovement();
		NextSelections();
		CheckSelection();
	}

	void PickFirstSelection()
	{
		if(firstSelection != null)
		{
			ChangeSelection(firstSelection);
			NextSelections();
		}
		else if(autoSelect)
		{
			for(int i=0; i < selectables.Length; i++)
			{
				if(selectables[i]!=null && selectables[i].interactable)
				{
					ChangeSelection(selectables[i]);
					NextSelections();
					return;
				}
			}
		}
	}

	void NextSelections()
	{
		if(currentSelectedObject == null) return;

		if(currentSelectedObject.FindSelectableOnUp()!= null)
		{
			for(int i = 0; i < selectables.Length; i++)
			{
				if(currentSelectedObject.FindSelectableOnUp() == selectables[i])
				{
					selectableUp = currentSelectedObject.FindSelectableOnUp();
					break;
				}
			}
		}
		else
		{
			selectableUp = null;
		}

		if(currentSelectedObject.FindSelectableOnDown() != null)
		{
			for(int i = 0; i < selectables.Length; i++)
			{
				if(currentSelectedObject.FindSelectableOnDown() == selectables[i])
				{
					selectableDown = currentSelectedObject.FindSelectableOnDown();
					break;
				}
			}
		}
		else
		{
			selectableDown = null;
		}

		if(currentSelectedObject.FindSelectableOnLeft() != null)
		{
			for(int i = 0; i < selectables.Length; i++)
			{
				if(currentSelectedObject.FindSelectableOnLeft() == selectables[i])
				{
					selectableLeft = currentSelectedObject.FindSelectableOnLeft();
					break;
				}
			}
		}
		else
		{
			selectableLeft = null;
		}

		if(currentSelectedObject.FindSelectableOnRight() != null)
		{
			for(int i = 0; i < selectables.Length; i++)
			{
				if(currentSelectedObject.FindSelectableOnRight() == selectables[i])
				{
					selectableRight = currentSelectedObject.FindSelectableOnRight();
					break;
				}
			}
		}
		else
		{
			selectableRight = null;
		}
	}

	void CheckSelection()
	{
		if(EventSystem.current.currentSelectedGameObject == null) return;
		for(int i = 0; i < selectables.Length; i++)
		{
			if(EventSystem.current.currentSelectedGameObject == selectables[i].gameObject)
			{
				EventSystem.current.SetSelectedGameObject(null);
				ChangeSelection(selectables[i]);
			}
		}
	}

	void CheckMovement()
	{
        Vector2 moveVector = new Vector2();

        if (allPlayers)
        {
            foreach (Player player in ReInput.players.AllPlayers)
            {
                moveVector += new Vector2(player.GetAxisRaw("Horizontal"), player.GetAxisRaw("Vertical"));
            }
        }
        else
        {
            moveVector = new Vector2(targetPlayer.GetAxisRaw("Horizontal"), targetPlayer.GetAxisRaw("Vertical"));
        }

		bool allow;

		if((moveVector.x >= deadzone || moveVector.x <= -deadzone || moveVector.y <= -deadzone || moveVector.y >= deadzone) && (Time.time-timer >= inputActionsDuration))
		{
			allow = true;
			timer = Time.time;
		}
		else if(moveVector.x <= deadzone && moveVector.x >= -deadzone && moveVector.y >= -deadzone && moveVector.y <= deadzone)
		{
			timer = Time.time-inputActionsDuration;
			allow = true;

		}
		else
		{
			allow = false;
		}

		if(allow)
		{
			if(moveVector.y >= deadzone)
			{
				if(selectableUp != null)
				{
					ChangeSelection(selectableUp);
				}
			}
			
			if(moveVector.y <= -deadzone)
			{
				if(selectableDown != null)
				{
					ChangeSelection(selectableDown);
				}
			}
			
			if(moveVector.x <= -deadzone)
			{
				if(selectableLeft != null)
				{
					ChangeSelection(selectableLeft);
				}
			}
			
			if(moveVector.x >= deadzone)
			{
				if(selectableRight != null)
				{
					ChangeSelection(selectableRight);
				}
			}
		}
	}

	bool ChangeSelection(Selectable nextSelectable)
	{
		if(nextSelectable == null) return false;

		if(currentSelectedObject!=null)
		{
			ExecuteEvents.Execute<IDeselectHandler>(currentSelectedObject.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.deselectHandler);
		}

		ExecuteEvents.Execute<ISelectHandler>(nextSelectable.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.selectHandler);
		currentSelectedObject = nextSelectable;

        OnInputReceived.Invoke();

		return true;
	}

	bool SendSubmitEventToSelectedObject()
	{
		if(currentSelectedObject == null) return false;

		ExecuteEvents.Execute<ISubmitHandler>(currentSelectedObject.gameObject,new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);
		return true;
	}

	//public void ChangeController(int newControllerID)
	//{
	//	controllerNumber = newControllerID;
	//	if (ReInput.players.GetPlayer (controllerNumber) != null) {
	//		player = ReInput.players.GetPlayer (controllerNumber);
	//	}
	//}
}
