﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTrigger : MonoBehaviour {

    //private Collider2D playerCollider;

    [System.Serializable] public class TriggerEvent : UnityEvent<int,GameObject> { }
    public TriggerEvent OnEnterTrigger = new TriggerEvent();
	public TriggerEvent OnStayTrigger = new TriggerEvent();
	public TriggerEvent OnExitTrigger = new TriggerEvent();

	[System.Serializable]
	public class CollideEvent : UnityEvent<GameObject> { }
	public CollideEvent OnCollideTrigger = new CollideEvent();

	private void Awake() {
        //playerCollider = GetComponent<Collider2D>();
    }

	private void OnCollisionEnter2D(Collision2D collision) {
		if (collision.gameObject.tag == "Player") {
			OnCollideTrigger.Invoke(collision.gameObject);
			Debug.Log("COLLIDE step 1");
		}
	}

	private void OnTriggerEnter2D(Collider2D collider) {
		if (collider.tag == "Trigger") {
			OnEnterTrigger.Invoke(collider.GetComponent<TriggerZone>().targetPlayerNumber, collider.gameObject);
		}
	}
	private void OnTriggerStay2D(Collider2D collider) {
        if (collider.tag == "Trigger") {
            OnStayTrigger.Invoke(collider.GetComponent<TriggerZone>().targetPlayerNumber, collider.gameObject);
        }
    }
	private void OnTriggerExit2D(Collider2D collider) {
		if (collider.tag == "Trigger") {
			OnExitTrigger.Invoke(collider.GetComponent<TriggerZone>().targetPlayerNumber, collider.gameObject);
		}
	}


}
